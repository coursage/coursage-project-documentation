# Coursage

_Coursage_ - облачный агрегатор курсов. Предоставляет инструментарий поиска курсов для пользователей
и CMS для администрации сервиса. В проекте применена микросервисная архитектура.

Актуальная версия сервиса развернута по адресу https://coursage.ru/

## Структура проекта

### Frontend

В проектах используются следующие технологии:

- _npm_ для сборки
- _gulp_ для разделения на компоненты
- _bootstrap_ для упрощения адаптивной верстки

| Название проекта                                                                                       | Описание                           |
|--------------------------------------------------------------------------------------------------------|------------------------------------|
| [Coursage customer frontend](https://gitlab.com/coursage/coursage-customer-frontend)                   | Фронтенд для рядовых пользователей |
| [Coursage course management frontend](https://gitlab.com/coursage/coursage-course-management-frontend) | Фронтенд для администрации сервиса |

### Backend

В проектах используются следующие технологии:

- _Java 19_
- _Maven_
- _Spring_
    - _Boot_
    - _Data_
    - _Web_
    - _Security_
    - _Cloud_
        - _Eureka_
        - _Config_
        - _Openfeign_
- _Lombok_
- _Postgres_
- _Flyway_

| Название проекта                                                                                     | Описание                                                                      |
|------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------|
| [Coursage registry service](https://gitlab.com/coursage/coursage-registry-service)                   | Реестр сервисов (сервер имен Eureka)                                          |
| [Coursage config server](https://gitlab.com/coursage/coursage-config-server)                         | Сервер конфигураций. Обеспечивает доступ к конфигурациям проектов по имени    |
| [Coursage configuration develop](https://gitlab.com/coursage/coursage-configuration-develop)         | Публичная конфигурация dev-окружения                                          |
| [Coursage course management backend](https://gitlab.com/coursage/coursage-course-management-backend) | Reverse proxy сервис для запросов от фронтенда для управления курсами         |
| [Coursage customer backend](https://gitlab.com/coursage/coursage-customer-backend)                   | Reverse proxy сервис для запросов от фронтенда для поиска и фильтрации курсов |
| [Coursage statistic service](https://gitlab.com/coursage/coursage-statistic-service)                 | Сервис обработки статистики поиска и кликов по курсам                         |
| [Coursage course service](https://gitlab.com/coursage/coursage-course-service)                       | Сервис работы с курсами (CRUD)                                                |

## Схема взаимодействия сервисов

![Coursage scheme.jpg](Coursage%20scheme.jpg)

## Инструкция по сборке и развертыванию

Перед выполнением инструкции убедитесь, что у вас установлены _git_ и _docker_.

### Подготовка

На этом этапе нам нужно скачать исходники и скрипты. Для этого выполните следующий скрипт:

```shell
git clone https://gitlab.com/coursage/coursage-config-server.git coursage/coursage-config-server
git clone https://gitlab.com/coursage/coursage-registry-service coursage/coursage-registry-service
git clone https://gitlab.com/coursage/coursage-course-management-backend.git coursage/coursage-course-management-backend
git clone https://gitlab.com/coursage/coursage-course-management-frontend.git coursage/coursage-course-management-frontend
git clone https://gitlab.com/coursage/coursage-customer-backend.git coursage/coursage-customer-backend
git clone https://gitlab.com/coursage/coursage-customer-frontend.git coursage/coursage-customer-frontend
git clone https://gitlab.com/coursage/coursage-statistic-service.git coursage/coursage-statistic-service
git clone https://gitlab.com/coursage/coursage-course-service.git coursage/coursage-course-service

cd coursage

curl -O "https://gitlab.com/coursage/coursage-project-documentation/-/raw/main/docker-compose.yml"

curl -O --create-dirs --output-dir "docker-postgresql-multiple-databases" "https://gitlab.com/coursage/coursage-project-documentation/-/raw/main/create-multiple-postgresql-databases.sh"
```

На линуксе могут возникать проблемы с доступом к файлам, поэтому даем разрешения:

```shell
chmod -R 777 ./
```

### Сборка и запуск

Создаем и запускаем докер контейнеры (запускать следует из директории _coursage_, куда скачался _docker-compose.yml_):

```shell
docker-compose up -d --build
```

После запуска контейнеров понадобится некоторое время на окончательный запуск и инициализацию.

### Что и где запустилось?

- Coursage course management frontend - http://localhost:33500/ (Логин: CoursageUserLogin, Пароль: CoursageUserPassword)
- Coursage customer frontend - http://localhost:33600/


